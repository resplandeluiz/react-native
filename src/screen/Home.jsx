import React, { useEffect, useState } from 'react';
import { Image, Button, StyleSheet, Text, View, FlatList, Modal } from 'react-native';
import StateComponent from '../components/StateComponent';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Form from '../components/Form';

const Home = ({ navigation }) => {

    const [fotos, setFotos] = useState(null);

    const baseUrl = "http://10.0.2.2:3000";
    const [modalVisibilidae, setModalVisibilidade] = useState(false);

    const callBack = () => { console.log(`Chamando função pai`); }

    useEffect(() => {
        carregarDados();
    }, [])

    async function carregarDados() {
        const data = await fetch(`${baseUrl}/v1/fotos`);
        const dataJson = await data.json();
        setFotos(dataJson);
    }

    async function deletarFoto(id) {
        await fetch(`${baseUrl}/v1/fotos/${id}`, { method: "DELETE" });
        carregarDados();
    }

    const closeModal = () => { setModalVisibilidade(false); carregarDados(); }

    const renderFoto = (foto) => {
        return (
            <View styles={styles.container}>
                <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "flex-end" }}>
                    <TouchableOpacity style={{ padding: 5, backgroundColor: "red", marginHorizontal: 50, borderRadius: 10, marginBottom: 5 }} onPress={() => { deletarFoto(foto.item._id) }}>
                        <Text style={{ color: "#FFFFFF", textTransform: "uppercase" }}>Excluir</Text>
                    </TouchableOpacity>
                </View>
                <TouchableOpacity onPress={() => { navigation.navigate('Details', { id: foto.item._id }) }}>
                    <View style={[styles.container, { marginBottom: 20 }]}>
                        <Image source={{ uri: foto.item.url }} style={{ width: '80%', height: 200, resizeMode: 'stretch' }} />
                        <Text style={{ fontSize: 16, marginVertical: 5, marginRight: 5 }}>{foto.item.titulo}</Text>
                        <Text style={{ fontSize: 10, marginVertical: 5 }}>{foto.item.descricao}</Text>
                    </View>
                </TouchableOpacity>
            </View>
        );
    }

    return (
        <View style={styles.container}>
            <View style={{ flexDirection: "row" }}>
                <TouchableOpacity style={styles.button} onPress={() => { carregarDados() }}>
                    <Text style={{ color: "#FFFFFF", textTransform: "uppercase" }}>Recarregar</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.button} onPress={() => { setModalVisibilidade(true) }}>
                    <Text style={{ color: "#FFFFFF", textTransform: "uppercase" }}>Cadastrar</Text>
                </TouchableOpacity>
            </View>

            {/* <StateComponent state={false} callBack={callBack.bind(this)} /> */}

            {fotos &&
                <FlatList
                    data={fotos}
                    style={{ width: '100%' }}
                    renderItem={foto => renderFoto(foto)}
                    keyExtractor={foto => foto._id}
                />
            }

            <Modal visible={modalVisibilidae} onRequestClose={() => { setModalVisibilidade(false) }} animationType={"slide"} children={<Form callBack={closeModal.bind(this)} />} />
        </View>
    );
}

export default Home;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    button: { padding: 10, backgroundColor: "blue", borderRadius: 8, margin: 5 }
});
