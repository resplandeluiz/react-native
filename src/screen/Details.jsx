import React, { useEffect, useState } from 'react';
import { StyleSheet, Modal, Text, View, Image, ActivityIndicator, Button } from 'react-native';
import { useRoute } from '@react-navigation/native';
import Form from '../components/Form';

const Details = ({ navigation }) => {

    const baseUrl = "http://10.0.2.2:3000";
    const route = useRoute();
    const [foto, setFoto] = useState(null);
    const [modalVisibilidae, setModalVisibilidade] = useState(false);

    useEffect(() => { carregarDetalhes(); }, []);

    async function carregarDetalhes() {
        const data = await fetch(`${baseUrl}/v1/fotos/${route.params.id}`);
        const dataJson = await data.json();
        setFoto(dataJson);
    }

    const statusModal = () => {
        setModalVisibilidade(false);
        carregarDetalhes();
    }
    return (
        <View style={styles.container} >
            {foto != null ?
                <>
                    <Image source={{ uri: foto.url }} style={{ width: '80%', height: 200, resizeMode: 'stretch' }} />
                    <Text style={styles.welcome}>{foto.titulo}</Text>
                    <Text>{foto.descricao}</Text>

                    <View style={{ alignItems: "flex-start" }}>
                        <Button title="Editar" onPress={() => { setModalVisibilidade(true) }} />
                    </View>

                    <Modal visible={modalVisibilidae} onRequestClose={() => { setModalVisibilidade(false); carregarDetalhes(); }} animationType={"slide"} children={<Form item={foto} callBack={statusModal.bind(this)} />} />

                </>
                : <ActivityIndicator colo={"#363636"} />}
        </View>
    );
}


export default Details;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});
