import React, { useEffect, useState } from 'react';
import { Text, StyleSheet, View, TextInput, Button, Image, Alert } from 'react-native';
import { useRoute } from '@react-navigation/native';

const Form = (props) => {

    const baseUrl = "http://10.0.2.2:3000";

    const { item = null, callBack = null } = props;

    const [titulo, setTitulo] = useState(item != null ? item.titulo : "");
    const [descricao, setDescricao] = useState(item != null ? item.descricao : "");
    const [url, setUrl] = useState(item != null ? item.url : "");


    const returnCallBack = () => {
        callBack != null ? callBack() : callBack;
    }

    async function atualizarFoto() {

        const esc = encodeURIComponent;
        const id = item != null ? item._id : Math.random().toString().slice(2);
        const formBody = { titulo: titulo, descricao: descricao, url: url, _id: id };
        const query = Object.keys(formBody).map(k => esc(k) + '=' + esc(formBody[k])).join('&');
        const uri = item != null ? `/v1/fotos/${item._id}` : `/v1/fotos`;

        await fetch(`${baseUrl}${uri}`, {
            method: item != null ? "PUT" : "POST",
            headers: { "Content-Type": "application/x-www-form-urlencoded" },
            body: query
        });

    }

    return (
        <View style={styles.container}>
            <Text>Titulo</Text>
            <TextInput
                style={{ width: '100%', marginVertical: 20, height: 40, borderColor: 'gray', borderWidth: 1 }}
                onChangeText={titulo => setTitulo(titulo)}
                value={titulo}
            />
            <Text>Descricao</Text>
            <TextInput
                style={{ width: '100%', marginVertical: 20, height: 40, borderColor: 'gray', borderWidth: 1 }}
                onChangeText={descricao => setDescricao(descricao)}
                value={descricao}
            />
            <Text>Uri</Text>
            <TextInput
                style={{ width: '100%', marginVertical: 20, height: 40, borderColor: 'gray', borderWidth: 1 }}
                onChangeText={url => setUrl(url)}
                value={url}
            />

            <Button title={item != null ? "Atualizar" : "Cadastrar"} onPress={() => { atualizarFoto(); returnCallBack(); }} />

        </View>
    );
}

export default Form;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginHorizontal: 20
    }
})