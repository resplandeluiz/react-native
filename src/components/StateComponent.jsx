import React, { useEffect } from "react";
import { Text } from 'react-native';

const StateCompnent = (props) => {

    const { state = false, callBack = null } = props;

    return (
        <Text onPress={callBack}> Seu estado é : {state ? "True" : "False"}</Text>
    );

}

export default StateCompnent;