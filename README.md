# Guia de instalação

Para rodar o projeto faça os seguintes paços

## Programas necessários

#
  Android Studio [Download](https://developer.android.com/studio) e configure um [Virtual Device](https://developer.android.com/studio/run/managing-avds)
  
  NodeJS [Download](https://nodejs.org/en/download/)

```bash
sudo apt install curl

curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -

sudo apt install nodejs
```
#
  NPM [Download](https://nodejs.org/en/download/) 
```bash  
curl -L https://npmjs.org/install.sh | sudo sh
```
   YARN [Download](https://classic.yarnpkg.com/en/docs/install/#debian-stable)
```bash  
sudo apt update && sudo apt install yarn
```
#
   Expo [Download](https://docs.expo.io/versions/latest/get-started/installation/)
```bash  
npm install -g expo-cli
```

Iniciar o projeto [link no git](https://gitlab.com/resplandeluiz/react-native)
```bash 
git clone

cd react-native/ 

yarn or npm i

expo start
```







